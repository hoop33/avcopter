var arDrone = require('ar-drone');
var client = arDrone.createClient();

var done = false;

client.config('control:alititude_max', 3000);
client.on('navdata', function(data) {
  if (done || !data || !data.demo) {
    console.log("done");
    return;
  }
  if (data.demo.altitudeMeters > 3) {
    console.log("Exceeded altitude limit");
    client.land();
    done = true;
  }
  if (data.demo.altitudeMeters < 2.5) {
    client.up(1);
    console.log("going up : " + data.demo.altitudeMeters);
  } else {
    console.log("Time to Flip!");
    client.up(0);
    console.log('Flipping!');
    client.animate('flipRight', 1000);
    client.animateLeds('snakeGreenRed', 2, 7);
    client.after(5000, function() {
      console.log("Going down");
      this.stop();
      this.land();
    });
  }
});
client.takeoff();

