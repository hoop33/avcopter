var arDrone = require('ar-drone');
var express = require('express');
var app = express();
app.use(express.static('video'));

var server = app.listen(9999, function() {
});

require('dronestream').listen(server);
//require('dronestream').listen
var client = arDrone.createClient();

var stdin = process.openStdin();
stdin.setRawMode(true);
var data = {
  upRate: 0,
  rightRate: 0,
  forwardRate: 0,
  strafeRightRate: 0
};

var increase = function(rate) {
  rate += 0.25;
  if (rate > 1) {
    rate = 1;
  }
  return rate;
};

var decrease = function(rate) {
  rate -= 0.25;
  if (rate <  -1) {
    rate = -1;
  }
  return rate;
};

var navdata;

client.on('navdata', function(data) {
  navdata = data;
});

setInterval(function() {
  if (navdata && navdata.demo) {
    var msg = "Battery: " + navdata.demo.batteryPercentage+'%';
    if (navdata.droneState.lowBattery) {
      msg += " ******LOW BATTERY******";
    }
    msg += '\n';
    process.stdout.write(msg);
  }
}, 5000);

stdin.on('data', function(chunk) {
  chunk = String(chunk);
  switch (chunk) {
    case "t":
      process.stdout.write(client.takeoff()+'\n');
      client.animateLeds('doubleMissile', 2, 5);
      break;
    case "g":
      process.stdout.write(client.land()+'\n');
      break;
    case "i":
      data.upRate = increase(data.upRate);
      process.stdout.write(client.up(data.upRate)+'\n');
      break;
    case "k":
      data.upRate = decrease(data.upRate);
      process.stdout.write(client.up(data.upRate)+'\n');
      break;
    case "a":
      data.strafeRightRate = decrease(data.strafeRightRate);
      process.stdout.write(client.right(data.strafeRightRate)+'\n');
      break;
    case "d":
      data.strafeRightRate = increase(data.strafeRightRate);
      process.stdout.write(client.right(data.strafeRightRate)+'\n');
      break;
    case "w":
      data.forwardRate = increase(data.forwardRate);
      process.stdout.write(client.front(data.forwardRate)+'\n');
      client.animateLeds('blinkGreen', 2, 5);
      break;
    case "s":
      data.forwardRate = decrease(data.forwardRate);
      process.stdout.write(client.front(data.forwardRate)+'\n');
      client.animateLeds('blinkRed', 2, 5);
      break;
    case "j":
      data.rightRate = decrease(data.rightRate);
      process.stdout.write(client.clockwise(data.rightRate)+'\n');
      client.animateLeds('leftMissile', 2, 5);
      break;
    case "l":
      data.rightRate = increase(data.rightRate);
      process.stdout.write(client.clockwise(data.rightRate)+'\n');
      client.animateLeds('rightMissile', 2, 5);
      break;
    case "x":
      data.upRate = 0;
      data.rightRate = 0;
      data.forwardRate = 0;
      data.strafeRightRate = 0;
      process.stdout.write(client.stop()+'\n');
      break;
    case "q":
      process.stdout.write(client.stop()+'\n');
      process.stdout.write("Goodbye\n");
      process.exit();
      break;
    case "r":
      process.stdout.write(client.disableEmergency()+'\n');
      break;
    case "p":
      process.stdout.write(JSON.stringify(navdata));
      break;
    case "1":
      process.stdout.write('Turning around\n');
      client.animate('turnaround', 3000);
      break;
    case "2":
      process.stdout.write("Do the wave!\n");
      client.animate("wave", 3000);
      break;
    case "3":
      process.stdout.write("Some theta yaw thingy");
      client.animate("theta20degYawM200deg", 3000);
      break;
    case "4":
      process.stdout.write("phi dance");
      client.animate('phiDance', 3000);
      break;
    case "z":
      process.stdout.write('Flip left!\n');
      client.animate('flipLeft', 1000);
      break;
    case "/":
      process.stdout.write('Flip right!\n');
      client.animate('flipRight', 1000);
      break;
    default:
      process.stdout.write("Unknown command\n");
      break;
  }
  process.stdout.write(JSON.stringify(data)+'\n');
});
