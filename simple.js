var arDrone = require('ar-drone');
var client = arDrone.createClient();

var done = false;

client.on('navdata', function(data) {
  if (done || !data || !data.demo) {
    return;
  }
  if (data.demo.altitudeMeters < 3) {
    client.up(0.5);
    console.log("going up : " + data.demo.altitudeMeters);
  } else {
    console.log("done!");
    client.up(0);
    done = true;
    client.after(5000, function() {
      console.log("Going down");
      this.stop();
      this.land();
    });
  }
});
client.takeoff();
/*client.after(5000, function() {
  this.clockwise(1);
}).after(3000, function() {
  this.stop();
  this.land();
});*/